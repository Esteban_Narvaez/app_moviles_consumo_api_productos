package com.utn.productoscrud;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import java.util.List;

public class ProductosAdapter extends RecyclerView.Adapter<ProductosAdapter.MyViewHolder> {

    private static List<Producto> productos;

    public ProductosAdapter(List<Producto> productos) {
        ProductosAdapter.productos = productos;
    }

    public void setProductos(List<Producto> productos) {
        ProductosAdapter.productos = productos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View productoView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_producto, parent, false);
        return new MyViewHolder(productoView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Producto producto = productos.get(position);
        holder.lbl1.setText(producto.titulo());
        holder.lbl2.setText(producto.detalle());
        Glide.with(holder.context).load(producto.image.secure_url).into(holder.imageView);
        holder.setOnLongClickListener(producto);
        holder.setOnClickListener(producto);
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        Context context;
        private final TextView lbl1, lbl2;
        private final ImageView imageView;
        private Producto productoSeleccionado;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            this.lbl1 = itemView.findViewById(R.id.textView);
            this.lbl2 = itemView.findViewById(R.id.textView2);
            this.imageView = itemView.findViewById(R.id.imageView);
            this.productoSeleccionado = new Producto();
        }

        private void setOnClickListener(Producto producto) {
            this.productoSeleccionado = producto;
            itemView.setOnClickListener(this);
        }

        public void setOnLongClickListener(Producto producto) {
            this.productoSeleccionado = producto;
            itemView.setOnLongClickListener(view -> onDelete());
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, MainActivity2.class);
            intent.putExtra("producto", this.productoSeleccionado);
            context.startActivity(intent);
        }

        public boolean onDelete() {
            String URL = "https://rest-api.klevervillalva.repl.co/products/" + productos.get(getAdapterPosition())._id;
            productos.remove(getAdapterPosition());
            notifyItemRemoved(getAdapterPosition());
            notifyItemRangeChanged(getAdapterPosition(), productos.size());
            StringRequest request = new StringRequest(Request.Method.DELETE, URL, response -> {
                Toast.makeText(context, response, Toast.LENGTH_SHORT).show();
            },error -> {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            });
            Volley.newRequestQueue(context).add(request);
            return true;
        }
    }
}
