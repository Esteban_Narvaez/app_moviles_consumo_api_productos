package com.utn.productoscrud;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;

public class MainActivity2 extends AppCompatActivity {

    EditText txtNombre, txtPrecio, txtDescripcion;
    Button btnGuardar;
    Spinner spinner;
    String modo = "creacion";
    Producto producto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        if (intent == null) finish();
        assert intent != null;
        producto = (Producto) intent.getExtras().getSerializable("producto");
        Calendar calendar = Calendar.getInstance();
        txtNombre = findViewById(R.id.editTextText);
        txtPrecio = findViewById(R.id.editTextText2);
        txtDescripcion = findViewById(R.id.editTextText3);
        btnGuardar = findViewById(R.id.button);
        spinner = findViewById(R.id.spinner);
        if (producto._id != null && !producto._id.equals("")){
            txtNombre.setText(producto.name);
            txtPrecio.setText(String.valueOf(producto.price));
            txtDescripcion.setText(producto.description);
            modo = "edicion";
        }
        btnGuardar.setOnClickListener(view -> {
            producto.name = txtNombre.getText().toString();
            producto.price = Integer.parseInt(txtPrecio.getText().toString());
            producto.description = txtDescripcion.getText().toString();
            producto.category = String.valueOf(spinner.getSelectedItem());
            producto.updatedAt = String.valueOf(calendar.getTime());
            Producto.Image image = new Producto.Image();
            image.public_id = "001";
            image.secure_url = "https://definicion.de/wp-content/uploads/2009/06/producto.png";
            producto.image = image;
            if(modo.equals("creacion")){
                producto.createdAt = String.valueOf(calendar.getTime());
                guardar();
            } else {
                actualizar();
            }
        });
    }

    private void guardar(){
        String URL = "https://rest-api.klevervillalva.repl.co/products";
        StringRequest request = new StringRequest(Request.Method.POST, URL, response -> {
            Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
            finish();
        },error -> {
            Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();
        }){
            public String getBodyContentType() {
            return "application/json; charset=utf-8";
        }
            @Override
            public byte[] getBody() {
                try {
                    JSONObject params = new JSONObject(producto.toString());
                    String requestBody = params.toString();
                    return requestBody.getBytes(StandardCharsets.UTF_8);
                } catch (JSONException e) {
                    Toast.makeText(MainActivity2.this, "Error conversión parámetros", Toast.LENGTH_SHORT).show();
                    Log.e("JSON EXCEPTION", "On getAllTurnos: " + e.getMessage());
                    return null;
                }
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

    private void actualizar(){
        String URL = "https://rest-api.klevervillalva.repl.co/products/" + producto._id;
        StringRequest request = new StringRequest(Request.Method.PUT, URL, response -> {
            Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
            finish();
        },error -> {
            Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();
        }){public String getBodyContentType() {
            return "application/json; charset=utf-8";
        }
            @Override
            public byte[] getBody() {
                try {
                    JSONObject params = new JSONObject(producto.toString());
                    String requestBody = params.toString();
                    return requestBody.getBytes(StandardCharsets.UTF_8);
                } catch (JSONException e) {
                    Toast.makeText(MainActivity2.this, "Error conversión parámetros", Toast.LENGTH_SHORT).show();
                    Log.e("JSON EXCEPTION", "On getAllTurnos: " + e.getMessage());
                    return null;
                }
            }
        };
        Volley.newRequestQueue(this).add(request);
    }
}