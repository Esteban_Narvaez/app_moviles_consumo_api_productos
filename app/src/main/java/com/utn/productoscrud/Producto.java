package com.utn.productoscrud;
import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.io.Serializable;

public class Producto implements Serializable {
    public Image image;
    public String _id;
    public String name;
    public String category;
    public String description;
    public int price;
    public String createdAt;
    public String updatedAt;
    public int __v;

    public static class Image implements Serializable{
        public String public_id;
        public String secure_url;

        @NonNull
        @Override
        public String toString() {
            return "{" +
                    "public_id='" + public_id + '\'' +
                    ", secure_url='" + secure_url + '\'' +
                    '}';
        }
    }

    public String titulo(){
        return name + "  $" + price;
    }

    public String detalle(){
        return description + "  Cat: " + category;
    }

    public static Producto fromJson(String json) {
        return new Gson().fromJson(json, Producto.class);
    }

    @NonNull
    @Override
    public String toString() {
        return "{" +
                "image=" + image.toString() +
                ", _id='" + _id + '\'' +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", __v=" + __v +
                '}';
    }
}
