package com.utn.productoscrud;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProductosAdapter adapter;
    FloatingActionButton btnNuevo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.lstProds);
        btnNuevo = findViewById(R.id.floatingActionButton);
        adapter = new ProductosAdapter(new ArrayList<>());
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        btnNuevo.setOnClickListener(view -> {
            Intent intent = new Intent(this, MainActivity2.class);
            intent.putExtra("producto", new Producto());
            startActivity(intent);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        obtenerProductos();
    }

    private void obtenerProductos(){
        List<Producto> productos = new ArrayList<>();
        String URL = "https://rest-api.klevervillalva.repl.co/products";
        JsonArrayRequest request = new JsonArrayRequest(URL, response -> {
            try {
                for (int i = 0; i < response.length(); i++) {

                    JSONObject resJSON = response.getJSONObject(i);
                    String resString = resJSON.toString();
                    Producto producto = Producto.fromJson(resString);

                    if (producto.image == null || producto.image.public_id == null){
                        Producto.Image image = new Producto.Image();
                        image.public_id = "001";
                        image.secure_url = "https://definicion.de/wp-content/uploads/2009/06/producto.png";
                        producto.image = image;
                    }
                    productos.add(producto);
                }
            } catch (JSONException e) {
                Toast.makeText(this, "Error conversión respuesta", Toast.LENGTH_SHORT).show();
            } finally {
                cargarLista(productos);
            }
        }, error -> {
            Toast.makeText(this, "Error en obtenerProductos: " + error.toString(), Toast.LENGTH_SHORT).show();
        });
        Volley.newRequestQueue(this).add(request);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void cargarLista(List<Producto> productos){
        if (adapter == null) return;
        if (productos == null) return;
        adapter.setProductos(productos);
        adapter.notifyDataSetChanged();
    }
}